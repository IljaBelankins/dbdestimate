# Running
To run the app, first start up a database with
```
docker-compose -f docker-compose.yaml up
```

In another terminal,

```
$env:DATABASE_PASSWORD = 'docker'
python manage.py runserver
```

# Migrations
To create a new migration firstly run:
```
$env:DATABASE_PASSWORD = 'docker'
python manage.py makemigrations killer_estimate
```

To run a migration:
```
python manage.py migrate
```