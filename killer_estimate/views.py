from django.shortcuts import render
from django.http import JsonResponse
from .models import Build, Killer, Perk
from rest_framework.request import Request
from rest_framework.parsers import JSONParser
from rest_framework.decorators import parser_classes, api_view
from rest_framework.exceptions import MethodNotAllowed, ParseError

def index(request):
    return render(request, 'home.html')

#Grabs all the information for a specific killer.
@api_view(["GET"])
@parser_classes((JSONParser, ))
def get_killer(request: Request, id: str):
    if request.method == 'GET':
        killer_id= id
        if killer_id is None:
            raise ParseError(detail="Killer info must be provided.")
        else:
            killer = Killer.retrieve_info(killer_id)
            return JsonResponse(killer.to_dict(), safe=False)
    else:
        raise MethodNotAllowed(request.method, detail=None, code=None)


@api_view(["POST"])
@parser_classes((JSONParser, ))
def post_killer(request: Request):
    if request.method == 'POST':
        killer_id= request.query_params.get("id")
        if killer_id is None:
            raise ParseError(detail="Killer info must be provided.")
        else:
            killer = Killer.retrieve_info(killer_id)
            return JsonResponse(killer.to_dict(), safe=False)
    else:
        raise MethodNotAllowed(request.method, detail=None, code=None)

#Gets all the data on all the killers

@api_view(["GET"])
@parser_classes((JSONParser, ))
def get_killer_preview(request: Request):
    if request.method == 'GET':
        data = []
        killers = Killer.objects.order_by("order")
        for killer in killers:
            item = killer.to_dict()
            data.append(item)
        return JsonResponse(data, safe=False)
    else:
        raise MethodNotAllowed(request.method, detail=None, code=None)

#This gets the data for a 'build', a preset combination of a killer
#and 4 perks.
@api_view(["GET", "POST"])
@parser_classes((JSONParser, ))
def build(request: Request):
    if request.method == 'GET':
        build_id= request.query_params.get("id")
        if build_id is None:
            raise ParseError(detail="Killer info must be provided.")
        else:
            build = Build.retrieve_info(build_id)
            return JsonResponse(build.to_dict(), safe=False)
    if request.method == 'POST':
        name = request.data.get("name")
        description = request.data.get("description")
        killer = request.data.get("killer", None)
        type = request.data.get("type")
        perks = request.data.get("perks")
        if name is None or perks is None or description is None or killer is None or type is None:
            raise ParseError(detail="Name, Perks, Description and Killer must be provided.")
        
        Build.create_build(name, description, killer, type, perks)
    else:
        raise MethodNotAllowed(request.method, detail=None, code=None)

'''
@api_view(["POST"])
@parser_classes((JSONParser))
def post_build(request: Request):
    if request.method == 'POST':
        name = request.data.get("name")
        description = request.data.get("description")
        killer = request.data.get("killer", None)
        type = request.data.get("type")
        perks = request.data.get("perks")
        if name is None or perks is None or description is None or killer is None or type is None:
            raise ParseError(detail="Name, Perks, Description and Killer must be provided.")
        
        Build.create_build(name, description, killer, type, perks)

    else:
        raise MethodNotAllowed(request.method, detail=None, code=None)
'''

#Retrieves all the perk data available.
@api_view(["GET"])
@parser_classes((JSONParser, ))
def get_perks(request: Request):
    if request.method == 'GET':
        perks = Perk.objects.all().order_by('name')
        perk_data = [perk.to_dict() for perk in perks]
        return JsonResponse(perk_data, safe=False)
    else:
        raise MethodNotAllowed(request.method, detail=None, code=None)
