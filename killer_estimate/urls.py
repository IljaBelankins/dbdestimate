from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('killer/<str:id>', views.get_killer, name='get_killer'),
    path('killer', views.get_killer_preview, name='killer_preview'),
    path('build', views.build, name='build'),
    path('perks', views.get_perks, name='perks')
]