import uuid
from django.db.utils import IntegrityError
from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator 
from typing import List

'''
The killer table that includes their name, unique power, images of the killer, as well as their
estimated strength in the 5 categories that define a killer's playstyle.
These include: Lethality (how quickly the killer incapacitates survivors)
Stalling (how long the killer can prolong the game. Time works in favour of the killer in the game)
Mobility (how fast the killer can traverse the map)
Detection (how easily the killer can find survivors who are hiding)
Consistency (how consistently can the killer use their power to great effect)
The values go from 1-100, 1 meaning ineptitude in said category or 100 meaning mastery in it.
'''
class Killer(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    order = models.IntegerField(null=True)
    name = models.CharField(max_length=50, unique=True, null=False, db_index=True)
    lethality = models.IntegerField(default=10, validators=[MinValueValidator(1), MaxValueValidator(100)])
    stalling = models.IntegerField(default=10, validators=[MinValueValidator(1), MaxValueValidator(100)])
    mobility = models.IntegerField(default=10, validators=[MinValueValidator(1), MaxValueValidator(100)])
    detection = models.IntegerField(default=10, validators=[MinValueValidator(1), MaxValueValidator(100)])
    consistency = models.IntegerField(default=10, validators=[MinValueValidator(1), MaxValueValidator(100)])
    avatar_src = models.CharField(max_length=100000)
    model_src = models.CharField(max_length=100000)
    power_src = models.CharField(max_length=100000)
    power_description = models.CharField(max_length=100000)

    def to_dict(self):
        return {
            "id": self.id,
            "name": self.name,
            "lethality": self.lethality,
            "stalling": self.stalling,
            "mobility": self.mobility,
            "detection": self.detection,
            "consistency": self.consistency,
            "avatar_src": self.avatar_src,
            "model_src": self.model_src,
            "power_src": self.power_src,
            "power_description": self.power_description
            }

    def to_simple_dict(self):
        return {
            "id": self.id,
            "name": self.name,
            "avatar_src": self.avatar_src,
            }

    @staticmethod
    def retrieve_info(killer_id: uuid):
        try:
            killer = Killer.objects.get(id=killer_id)
        except Killer.DoesNotExist:
            raise ValueError("Invalid id.")
        return killer


'''
Perks are effects the killer can equip before the game to help their performance.
Up to 4 can be selected, and they boost certain aspects of their playstyle a little bit.
The perks also are rated in same  5 performance categories as the killer since they function
to improve the killer rather than grant them new abilities.
Perks also have a tip field, a piece of information that will be displayed for the user
to read to help them better understand how to utilise the perk most efficiently in-game.
'''
class Perk(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    killer = models.ForeignKey(Killer, on_delete=models.CASCADE, null=True, default=None)
    name = models.CharField(max_length=50, unique=True, null=False, db_index=True)
    perk_description = models.CharField(max_length=100000)
    lethality = models.IntegerField(default=1, validators=[MinValueValidator(1), MaxValueValidator(100)])
    stalling = models.IntegerField(default=1, validators=[MinValueValidator(1), MaxValueValidator(100)])
    mobility = models.IntegerField(default=1, validators=[MinValueValidator(1), MaxValueValidator(100)])
    detection = models.IntegerField(default=1, validators=[MinValueValidator(1), MaxValueValidator(100)])
    consistency = models.IntegerField(default=1, validators=[MinValueValidator(1), MaxValueValidator(100)])
    image_src = models.CharField(max_length=100000, null=True)
    tip = models.CharField(max_length=5000, null=True)
    

    def to_dict(self):
        return {
            "id": self.id,
            "killer_id": self.killer.id if self.killer else None,
            "name": self.name,
            "perk_description": self.perk_description,
            "lethality": self.lethality,
            "stalling": self.stalling,
            "mobility": self.mobility,
            "detection": self.detection,
            "consistency": self.consistency,
            "image_src": self.image_src,
            "tip": self.tip
            }

#This table is meant to represent pre-generated combinations of killer
#and perks to help players unfamiliar with the game select a playstyle
#they may enjoy quickly without having to research in their own time.
class Build(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=50, unique=True, null=False, db_index=True)
    build_description = models.CharField(max_length=100000)
    type = models.CharField(max_length=50, null=False)
    killer = models.ForeignKey(Killer, on_delete=models.CASCADE, null=True, default=None)
    
    @property
    def perks(self):
        return [buildperk.perk for buildperk in self.buildperk_set.all().order_by("perk__name")]

    @staticmethod
    def create_build(name: str, build_description: str, killer: Killer, type: str, perk_ids: List[str]):
        build = Build(name=name, build_description=build_description, type=type, killer=killer)
        try:
            build.save()
        except IntegrityError as e:
            print (f'Caught exception while saving Build: {str(e)}')
            raise ValueError("Duplicate column value.")
        for perk_id in perk_ids:
            perk = Perk.objects.get(pk=perk_id)
            build_perk_object = BuildPerk(
                build = build,
                perk = perk
            )
            build_perk_object.save()
        return build


    def to_dict(self):
        return {
            "id": self.id,
            "name": self.name,
            "build_description": self.build_description,
            "type": self.type,
            "killer": self.killer.to_dict() if self.killer else None,
            "perks": [perk.to_dict() for perk in self.perks],
            }

#A pivot table to help build functionality.
class BuildPerk(models.Model):
    build = models.ForeignKey(Build, on_delete=models.CASCADE, null=False, default=None)
    perk = models.ForeignKey(Perk, on_delete=models.CASCADE, null=True, default=None)

