from django.apps import AppConfig


class KillerEstimateConfig(AppConfig):
    name = 'killer_estimate'
