from django.core.management.base import BaseCommand
from killer_estimate.models import Killer, Perk, Build, BuildPerk
from typing import List
import json

class Command(BaseCommand):
    help = 'insert killer data from json file containing a list of killer dictionaries'

    def handle(self, *args, **options):
        file = open("killer_estimate\management\commands\killer_data.json", "r")
        file_data = file.read()
        data = json.loads(file_data)
        for killer in data["killers"]:
            new_killer = Killer(
            name = killer["name"],
            lethality = killer["lethality"],
            stalling = killer["stalling"],
            mobility = killer["mobility"],
            detection = killer["detection"],
            consistency = killer["consistency"],
            avatar_src = killer["avatar_src"],
            model_src = killer["model_src"],
            power_src = killer["power_src"],
            power_description = killer["power_description"]
            )
            new_killer.save()