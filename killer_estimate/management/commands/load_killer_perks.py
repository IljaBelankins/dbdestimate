from django.core.management.base import BaseCommand
from killer_estimate.models import Killer, Perk
import json


class Command(BaseCommand):
    help = 'insert killer perk data from json file containing a list of killer dictionaries'

    def handle(self, *args, **options):
        file = open("killer_estimate\management\commands\killer_perk_data.json", "r")
        file_data = file.read()
        data = json.loads(file_data)
        for perk in data:
            print (perk['killer'])
            killer_name = perk['killer']
            if killer_name == "All":
                assigned_killer = None
            else:
                assigned_killer = Killer.objects.get(name=killer_name)
            new_killer_perk = Perk(
            name = perk['name'],
            perk_description = perk['description'],
            killer_id = assigned_killer,
            image_src = perk['image']
            )
            new_killer_perk.save()