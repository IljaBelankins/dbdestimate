import requests
import lxml.html as lh
import json

url ='https://deadbydaylight.fandom.com/wiki/Perks'
page = requests.get(url)
doc = lh.fromstring(page.content)
table_elements = doc.xpath('//table')
perk_table = table_elements[55]
tr_elements = perk_table.xpath('.//tr')

perks = []
for row in tr_elements[1:]:
    perk = {
        "name": None,
        "description": None,
        "killer": None,
        "image": None
    }
    perk["name"] = row.xpath('.//th')[1].text_content()
    perk["description"] = row.xpath('.//td')[0].text_content()
    perk["killer"] = row.xpath('.//th')[2].text_content()
    perk["image"] = row.xpath('.//img/@data-src')[0]
    perks.append(perk)
killer_perks = json.dumps(perks)

f = open("killer_perk_data.json", "w")
f.write(killer_perks)
f.close()
